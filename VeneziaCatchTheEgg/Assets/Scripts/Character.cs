using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    [SerializeField] private float moveSpeed;
    [SerializeField] private Transform pointA;
    [SerializeField] private Transform pointB;
    Rigidbody rb;
    float dirX;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        dirX = Input.acceleration.x * moveSpeed;
        transform.position = new Vector3(Mathf.Clamp(transform.position.x, pointA.position.x, pointB.position.x), transform.position.y, transform.position.z);
    }

    private void FixedUpdate()
    {
        rb.velocity = new Vector3 (dirX, 0f, 0f);
    }

  
}
