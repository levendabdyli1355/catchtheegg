using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Egg : MonoBehaviour
{
    private float myDrag;
    private Color myColor;
    private int Value;
    private int random;

    private GameManager gameManager;

    private void Awake()
    {
        gameManager = FindObjectOfType<GameManager>();
        random = Random.Range(0, 3);
        UpdateStats();
        SetupStats();
    }

    private void UpdateStats()
    {
        switch (random)
        {
            case 0:
                myColor = Color.white;
                myDrag = 3f;
                Value = 5;
                break;

            case 1:
                myColor = Color.cyan;
                myDrag = 4f;
                Value = 10;
                break;

            case 2:
                myColor = Color.magenta;
                myDrag = 5f;
                Value = 30;
                break;

            default:
                break;
        }
    }

    private void SetupStats()
    {
        GetComponent<Rigidbody>().drag = myDrag;
        GetComponentInChildren<SpriteRenderer>().color = myColor;
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            gameManager.AddScore(Value);
            Destroy(this.gameObject);
        }
        else if (other.CompareTag("DeadZone"))
        {
            Destroy(this.gameObject);
        }
    }
}
