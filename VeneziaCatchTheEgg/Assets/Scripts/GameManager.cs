using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Chiedo estremamente perdono recuperer˛ tutto al prossimo progetto personale

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }

    [SerializeField] Spawner spawner;
    [SerializeField] GameObject player;

    [SerializeField] Text _score;
    [SerializeField] Text _time;
    [SerializeField] Text _finalScore;

    [SerializeField] int roundDuration;

    private float timer;
    private int currMinutes;
    private int currSeconds;

    public int score;
    public int maxScore;
    public bool Loop = true;

    public int Score
    {
        get => score;
        set
        {
            score = value;
            Time.timeScale = 1 + score / 100f;
            _score.text = $"Score: {score}";
        }
    }

    public void AddScore(int value)
    {
        score += value;
    }

    private void Update()
    {
        if (Loop)
        {
            RoundTimeUpdate();
            _score.text = "Score :" + score.ToString();
        }
        else
        {
            _finalScore.text = "FINAL SCORE:" + score.ToString();
            PauseGame();
        }
    }

    void RoundTimeUpdate()
    {
        if (timer < roundDuration)
        {
            timer = Time.time;
            currMinutes = Mathf.FloorToInt(timer / 60F);
            currSeconds = Mathf.FloorToInt(timer - currMinutes * 60);
            _time.text = string.Format("Reach 1 Minute: {0:0}:{1:00}", currMinutes, currSeconds);
        }
        else
        {
            Loop = false;
        }
    }

    void PauseGame()
    {
        Time.timeScale = 0;
    }

}
