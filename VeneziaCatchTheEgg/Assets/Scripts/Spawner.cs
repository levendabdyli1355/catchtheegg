using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [Header("Spawner Settings")]
    [SerializeField] private Egg egg;
    [SerializeField] private float fireRate;
    [SerializeField] private float movementSpeed;
    
    [SerializeField] private Transform pointA;
    [SerializeField] private Transform pointB;

    private Transform target;
    private Rigidbody rb;

    private float lastShot;

    private float lastTurn;
    private float turnRate = 0;

    float random;

    private GameManager manager;

    private void Awake()
    {
        manager = FindObjectOfType<GameManager>();
        rb = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        if(manager.Loop)
        {
            random = Random.Range(pointA.position.x, pointB.position.x);
            Shoot();

        }
    }

    private void Shoot()
    {
        if(Time.time >= lastShot + fireRate)
        {
            Movement();
            lastShot = Time.time;
            Instantiate(egg, transform.position, transform.rotation);
        }
    }

    private void Movement()
    {
        transform.position = new Vector3(random, transform.position.y, transform.position.z);
        //if(Time.time >= lastTurn + turnRate)
        //{
        //    lastTurn = Time.time;
        //    turnRate = Random.Range(1, 3);
        //    ChangeDirection();
        //}
    }

    private void ChangeDirection()
    {
        if(target == pointA)
        {
            target = pointB;
        }
        else
        {
            target = pointA;
        }
    }

}
